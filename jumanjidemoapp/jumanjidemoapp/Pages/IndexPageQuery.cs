﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using jumanjidemoapp.Infrastructure.configserver;
using MediatR;
using Microsoft.Extensions.Options;
using Steeltoe.Extensions.Configuration;
using Steeltoe.Extensions.Configuration.CloudFoundry;

namespace jumanjidemoapp.Pages
{
    public class IndexPageQuery
    {
        public class Query : IRequest<QueryResult>
        {
            public int Id { get; set; }
        }
        public class QueryResult
        {
            public string Title { get; set; }
            public string WelcomeName { get; set; }
            public int Version { get; set; }
        }

        public class Handler : IRequestHandler<Query, QueryResult>
        {
            public Handler(IOptionsSnapshot<ConfigServerData> configServerData)
            {
                if (configServerData != null)
                    ConfigServerData = configServerData;
            }

            private IOptionsSnapshot<ConfigServerData> ConfigServerData { get; set; }
            public async Task<QueryResult> Handle(Query request, CancellationToken cancellationToken)
            {
                //var test = AppOptions.ApplicationName;
                // IConfigServerData property is set to a IOptionsSnapshot<ConfigServerData> that has been
                // initialized with the configuration data returned from the Spring Cloud Config Server
                ConfigServerData data = null;
                if (ConfigServerData?.Value != null)
                {
                    data = ConfigServerData.Value;
                }
                var resultData = new QueryResult
                {
                    Title = data?.Title ?? "Title Not returned",
                    WelcomeName = data?.Name ?? "Name Not returned",
                    Version = data?.Version ?? 0
                };
                await Task.Run(() => Console.WriteLine("This is Jungle Demo App"), cancellationToken);
                return resultData;
            }
        }
    }
}
