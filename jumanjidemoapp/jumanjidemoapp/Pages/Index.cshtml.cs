﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MediatR;

namespace jumanjidemoapp.Pages
{
    public class IndexModel : PageModel
    {
        public IndexPageQuery.QueryResult Data { get; private set; }
        private readonly IMediator _mediator;

        public IndexModel(IMediator mediator)
        {
            _mediator = mediator;
        }
        public async Task<IActionResult> OnGetAsync()
        {
            var query = new IndexPageQuery.Query {Id = 1};
            Data = await _mediator.Send(query);
            return Page();
        }
    }
}
