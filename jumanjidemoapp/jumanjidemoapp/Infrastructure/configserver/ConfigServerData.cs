﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jumanjidemoapp.Infrastructure.configserver
{
    public class ConfigServerData
    {
        public int Version { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
    }
}
